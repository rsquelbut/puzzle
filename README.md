# README #

Petit projet pour se rappeler que l'application d'une lambda 
n'est pas identique au passage de méthode par référence en Java 8.
En effet, dans certains cas (un pour le moment :) ) le passage par référence 
provoque une `IllegalArgumentException`.

# Il suffit de regarder le code de ce projet #
- C'est la visibilité _package_ de la classe `Civil` qui implique le comportement en erreur.
- C'est dans la classe `Main` qu'on pourra comparer 
    - le code qui provoque l'erreur `Main#identifiantJoinerByReference(...)` 
    - avec le code qui ne provoque pas l'erreur `Main#identifiantJoinerByLambda(...)`.
- Les classes `Nom` et `Prenom` sont inutiles.