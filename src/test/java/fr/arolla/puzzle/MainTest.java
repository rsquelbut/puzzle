package fr.arolla.puzzle;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.arolla.puzzle.core.Personne;

/**
 * Created by rsquelbut on 30/10/15.
 */
public class MainTest {
	private List<Personne> personnes;

	@Before
	public void init() {
		personnes = Arrays.asList(Personne.of(5l, "Georges", "5"),//
				Personne.of(14l, "Louis", "XIV"),//
				Personne.of(23l, "Jean", "XXIII"));
	}

	@Test
	public void testIdentifiantJoinerWithLambda() throws Exception {
		final String actual = new Main().identifiantJoinerWithLambda(personnes);
		final String expected = "5;14;23";
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testIdentifiantJoinerWithReference() throws Exception {
		final String actual = new Main().identifiantJoinerWithReference(personnes);
		final String expected = "5;14;23";
		Assert.assertEquals(expected, actual);
	}
}