package fr.arolla.puzzle.core;

/**
 * Created by rsquelbut on 30/10/15.
 */
public class Identifiant {
	private final long value;

	private Identifiant(final long value) {
		this.value = value;
	}

	public static Identifiant of(final long value) {
		return new Identifiant(value);
	}

	public long getValue() {
		return value;
	}
}
