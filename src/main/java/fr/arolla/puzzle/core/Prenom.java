package fr.arolla.puzzle.core;

/**
 * Created by rsquelbut on 30/10/15.
 */
public class Prenom {
	private final String value;

	private Prenom(final String value) {
		this.value = value;
	}

	public static Prenom of(String prenom) {
		return new Prenom(prenom);
	}

	public String getValue() {
		return value;
	}
}
