package fr.arolla.puzzle.core;

/**
 * Created by rsquelbut on 30/10/15.
 */
public class Personne extends Civil {
	private Prenom prenom;
	private Nom nom;

	public Personne(final Identifiant identifiant, final Nom nom, final Prenom prenom) {
		super(identifiant);
		this.nom = nom;
		this.prenom = prenom;
	}

	public static Personne of(final Identifiant identifiant, final Nom nom, final Prenom prenom) {
		return new Personne(identifiant, nom, prenom);
	}

	public static Personne of(final long identifiant, final String nom, final String prenom) {
		return new Personne(Identifiant.of(identifiant), Nom.of(nom), Prenom.of(prenom));
	}

	public Prenom getPrenom() {
		return prenom;
	}

	public void setPrenom(final Prenom prenom) {
		this.prenom = prenom;
	}

	public Nom getNom() {
		return nom;
	}

	public void setNom(final Nom nom) {
		this.nom = nom;
	}
}
