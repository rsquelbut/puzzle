package fr.arolla.puzzle.core;

/**
 * Created by rsquelbut on 30/10/15.
 */
 class Civil {
	        private    final Identifiant identifiant;

	public Civil(final Identifiant identifiant) {
		this.identifiant = identifiant;
	}

	public Identifiant getIdentifiant() {
		return identifiant;
	}
}
