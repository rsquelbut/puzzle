package fr.arolla.puzzle.core;

/**
 * Created by rsquelbut on 30/10/15.
 */
public class Nom {
	private final String value;

	private Nom(final String value) {
		this.value = value;
	}

	public static Nom of(String prenom) {
		return new Nom(prenom);
	}

	public String getValue() {
		return value;
	}
}
