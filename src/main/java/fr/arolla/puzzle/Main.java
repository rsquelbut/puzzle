package fr.arolla.puzzle;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.arolla.puzzle.core.Identifiant;
import fr.arolla.puzzle.core.Personne;

/**
 * Created by rsquelbut on 30/10/15.
 */
public class Main {

	public String identifiantJoinerWithReference(final List<Personne> personnes) {
		return personnes.stream()
				.map(Personne::getIdentifiant)
				.map(Identifiant::getValue)
				.map(String::valueOf)
				.collect(Collectors.joining(";"));
	}

	public String identifiantJoinerWithLambda(final List<Personne> personnes) {
		return personnes.stream()
				.map(p -> p.getIdentifiant())
				.map(Identifiant::getValue)
				.map(String::valueOf)
				.collect(Collectors.joining(";"));
	}
}
